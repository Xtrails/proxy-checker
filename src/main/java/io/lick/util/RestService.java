package io.lick.util;

import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.Map;

/**
 * Сервис для REST запросов
 */
public interface RestService {

    /**
     * Формирует ссылку для GET запроса
     * @param link - ссылка на ресурс
     * @param params - параметры GET запроса
     * @return - параметризованная ссылка для GET запроса
     */
    URL createURL(String link, Map<String, String> params);

    /**
     * Получить RestTemplate
     *
     * Гайд по использованию RestTemplate:
     * https://www.baeldung.com/rest-template
     * @return - RestTemplate
     */
    RestTemplate getRestTemplate();
}
