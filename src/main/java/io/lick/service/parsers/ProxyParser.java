package io.lick.service.parsers;

import org.threeten.bp.OffsetDateTime;

import java.util.Set;

/**
 * Универсальный интерфейс управления работой всех CPA-сервисов
 */
public interface ProxyParser {

    /**
     * Получить список офферов
     * @return - список офферов
     */
    Set<String> getProxySet();

    /**
     * Возвращает имя сервиса
     * @return - имя сервиса
     */
    String getServiceName();

    /**
     * Получить дату и время последнего обновления
     * @return - дата и время последнего обновления
     */
    OffsetDateTime getDateTimeLastUpdate();

    /**
     * Получить дату и время запуска сервиса
     * @return - дата и время запуска сервиса
     */
    OffsetDateTime getDateTimeStart();

    void start();

    void stop();

    boolean isRunning();

    void setThreadName(String threadName);
}
