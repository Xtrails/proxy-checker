package io.lick.service.manager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public abstract class ThreadMgr implements Runnable{

    private volatile Thread thread;
    private boolean running = false;
    private String threadName;

    /**
     * Запустить поток
     */
    public synchronized void start() {
        if (thread != null) {
            log.info(threadName + " поток уже запущен.");

            if (!isRunning()) {
                log.info("Resuming " + threadName + "...");
                if (thread != Thread.currentThread()) {
                    boolean stoppedRunningThread = false;
                    do {
                        try {
                            thread.join();
                            stoppedRunningThread = true;
                        } catch (InterruptedException ie) {
                            //ignore
                        }
                    } while (!stoppedRunningThread);
                    startApplicationThread();
                } else {
                    setRunning(true);
                }
            }
        } else {
            log.info("Запускаем поток " + threadName + "...");
            startApplicationThread();
            log.info("Поток " + threadName + " успешно запущен.");
        }
    }

    /**
     * Остановить сервис
     */
    public synchronized void stop() {
        log.info("Останавливаем поток " + threadName + "...");
        setRunning(false);
        log.info("Поток " + threadName + " успешно остановлен.");
    }

    /**
     * Запущен сервис или нет
     * @return true/false
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Запустить новый поток
     */
    private void startApplicationThread() {
        thread = new Thread(this, threadName);
        setRunning(true);
        thread.start();
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    private void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * Ожидание потока устоновленное количество секунд
     *
     * @param min - на сколько приостоновить поток
     * @throws InterruptedException
     */
    public void threadPause(int min){
        try {
            Thread.sleep(min * 60000L);
        } catch (InterruptedException e) {
            log.error("Interrupted!", e);
            Thread.currentThread().interrupt();
        }
    }
}
