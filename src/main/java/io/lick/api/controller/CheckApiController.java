package io.lick.api.controller;

import io.lick.util.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CheckApiController implements CheckApi {

    @Autowired
    private CheckService checkService;
    private final HttpServletRequest request;

    public CheckApiController(HttpServletRequest request) {
        this.request = request;
    }

    public ResponseEntity<Void> checkProxy(@RequestBody String proxyList) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
                checkService.check(proxyList);
                return new ResponseEntity<Void>(HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
