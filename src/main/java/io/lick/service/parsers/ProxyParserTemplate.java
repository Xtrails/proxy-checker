package io.lick.service.parsers;

import io.lick.configuration.properties.ProxyProperties;
import io.lick.service.manager.ThreadMgr;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Шаблон для создания сервисов по работе с CPA площадками
 *
 * Прмер многопоточности описан тут:
 * https://codereview.stackexchange.com/questions/118217/starting-and-stopping-a-thread-for-a-command-handler
 */
@Slf4j
@Service
@RequiredArgsConstructor
public abstract class ProxyParserTemplate extends ThreadMgr {

    private final ProxyProperties proxyProperties;

    /**
     * Дата и время последнего запуска сервиса
     **/
    private OffsetDateTime dateTimeStart = null;
    /**
     * Дата и время последнего получения прокси с сервера
     */
    private OffsetDateTime dateTimeLastUpdate = null;

    /**
     * Обновляемый каждые UPDATE_PERIOD_TIME секунд, список прокси
     */
    private List<String> proxyList = new LinkedList<>();

    /**
     * Работа сервиса
     *
     * @param offersURL        - ссылка для получения офферов
     * @param updatePeriodTime - период повторения
     */
    public void execute(URL offersURL, Integer updatePeriodTime) {
        if(updatePeriodTime == null || updatePeriodTime < 0)
            updatePeriodTime = proxyProperties.getUpdatePeriodTime();
        while (true) {
            updateProxy(offersURL);
            updateDateTimeLastUpdate();
            checkProxyList();
            threadPause(updatePeriodTime);
        }
    }

    /**
     * Обновить время последнего обновления прокси
     */
    public void updateDateTimeLastUpdate(){
        ZoneId zid = ZoneId.of(proxyProperties.getDateTimeZoneId());
        dateTimeLastUpdate = OffsetDateTime.now(zid);
    }

    /**
     * Обновить время запуска сервиса
     */
    public void updateDateTimeStart(){
        ZoneId zid = ZoneId.of(proxyProperties.getDateTimeZoneId());
        dateTimeStart = OffsetDateTime.now(zid);
    }

    /**
     * Ожидание потока устоновленное в параметрах количество секунд
     *
     * @throws InterruptedException
     */
    public void threadPause() throws InterruptedException {
        threadPause(proxyProperties.getUpdatePeriodTime());
    }

    /**
     * Проверить прокси лист на доступность
     */
    public abstract void checkProxyList();

    /**
     * Получить список прокси по ссылке
     *
     * @param url - ссылка на получение офферов
     */
    public abstract void updateProxy(URL url);

    /**
     * Формирует ссылку для запроса прокси
     *
     * @return - ссылка для запроса прокси
     */
    public abstract URL createGetProxyURL();

    public OffsetDateTime getDateTimeStart() {
        return dateTimeStart;
    }

    public void setDateTimeStart(OffsetDateTime dateTimeStart) {
        this.dateTimeStart = dateTimeStart;
    }

    public OffsetDateTime getDateTimeLastUpdate() {
        return dateTimeLastUpdate;
    }

    public void setDateTimeLastUpdate(OffsetDateTime dateTimeLastUpdate) {
        this.dateTimeLastUpdate = dateTimeLastUpdate;
    }
}
