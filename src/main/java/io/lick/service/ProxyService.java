package io.lick.service;

import io.lick.model.FilterForm;
import io.lick.model.ProxyResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

public interface ProxyService {

    /**
     * Получить отфильтрованный список проверенных прокси серверов
     * @param form - настройки фильтра
     * @return - отфильтрованный список проверенных прокси серверов
     */
    List<ProxyResponse> getProxyList(FilterForm form);

    /**
     * Добавить прокси в список для проверки
     * @param proxySet - список прокси
     */
    void addProxyUncheckSet(Set<String> proxySet);

    /**
     * Добавить список прокси в неактивные
     * @param deadSet - список неактивных прокси
     */
    void addProxyDeadSet(Set<String> deadSet);

    /**
     * Возвращает имя сервиса
     * @return - имя сервиса
     */
    String getServiceName();

    void start();

    void stop();

    boolean isRunning();

    void setThreadName(String threadName);
}
