package io.lick;

import io.lick.service.ProxyService;
import io.lick.service.parsers.FreeProxyListService;
import io.lick.service.parsers.HookzofService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "io.lick", "io.lick.api" , "io.lick.configuration"})
public class Swagger2SpringBoot implements CommandLineRunner {

    @Autowired
    HookzofService hookzofService;

    @Autowired
    FreeProxyListService freeProxyListService;

    @Autowired
    ProxyService proxyService;
    public static void main(String[] args) {
        SpringApplication.run(Swagger2SpringBoot.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        proxyService.start();

//        hookzofService.start();
        freeProxyListService.start();

    }
}
