package io.lick.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api(value = "check", description = "the check API")
public interface CheckApi {

    @ApiOperation(value = "Проверяет работоспособность прокси серверов", nickname = "checkProxy", notes = "", tags={ "All", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation") })
    @RequestMapping(value = "/check",
            produces = { "application/json" },
            consumes = { "text/html" },
            method = RequestMethod.POST)
    ResponseEntity<Void> checkProxy(@RequestBody String proxyList);

}
