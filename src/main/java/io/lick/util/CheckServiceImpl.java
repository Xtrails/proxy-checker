package io.lick.util;

import io.lick.configuration.properties.CheckerProperties;
import io.lick.model.Proxy;
import io.lick.model.ProxyResponse;
import io.lick.model.enums.StatusEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CheckServiceImpl implements CheckService {

    private java.net.Proxy.Type type = java.net.Proxy.Type.HTTP;

    private Map<String, ProxyResponse> checkMap = new HashMap<>();
    private Map<String, ProxyResponse> checkAliveMap = new HashMap<>();
    private Set<String> uncheckSet = new HashSet<>();
    private Set<String> deadSet = new HashSet<>();
    private final RequestAPI requestAPI;
    private final CheckerProperties checkerProperties;

    /**
     * Проверить список прокси адресов на доступность
     *
     * @param proxyList - список прокси серверов
     */
    public void check(Set<String> proxyList) {
        uncheckSet.addAll(proxyList);
        System.out.println("В список на проверку добавлено " + proxyList.size() + " прокси");
        System.out.println("Запускаем проверку...");
        start();
    }

    /**
     * Проверить список прокси адресов на доступность
     *
     * @param proxyList - список прокси серверов
     */
    @Override
    public void check(String proxyList) {
        Set<String> validProxySet = stringToProxyList(proxyList);
        if (validProxySet != null && !validProxySet.isEmpty()) {
            check(validProxySet);
        }
    }

    /**
     * Запустить проверку прокси из uncheckList
     */
    private void start() {
        ExecutorService executorService = Executors.newFixedThreadPool(checkerProperties.getMaxThreads());

        Iterator itr = uncheckSet.iterator();
        while (itr.hasNext()) {
            String proxy = (String) itr.next();
            executorService.submit(
                    new Checker(proxy)
            );
            itr.remove();
        }
    }

    /**
     * Task that asynchronous checks each proxy and updates the TableView
     */
    private class Checker implements Runnable {

        private String strProxy;
        private Proxy proxy;

        public Checker(String strProxy) {
            this.strProxy = strProxy;
        }


        @Override
        public void run() {

            this.proxy = new Proxy( // first creation of the Proxy object from the ListView
                    strProxy,
                    type
            );

            java.net.Proxy proxy = new java.net.Proxy(
                    this.proxy.getProxyType(),
                    new InetSocketAddress(
                            this.proxy.getIp(),
                            this.proxy.getPort()
                    )
            );

            Map.Entry<HttpURLConnection, Long> pair = requestAPI.connect(proxy);
            if (pair != null) {
                try {
                    this.proxy.setProxyStatus(StatusEnum.ALIVE);
                    RequestAPIImpl.Response response = requestAPI.getResponse(pair.getKey());
                    this.proxy.setProxyAnonymity(response.anonymity);
                    this.proxy.setCountry(response.country);
                    this.proxy.setResponseTime(pair.getValue() + " (ms)");

                } catch (Exception e) {
                    this.proxy.setProxyStatus(StatusEnum.DEAD);
                    this.proxy.setProxyAnonymity(null);
                }
            } else {
                this.proxy.setProxyStatus(StatusEnum.DEAD);
                this.proxy.setProxyAnonymity(null);
            }
            ProxyResponse response = new ProxyResponse(this.proxy);

            if (response.getStatus() != null) {
                if (response.getStatus().equals(StatusEnum.ALIVE)) {
                    addAliveCheckMap(strProxy, response);
                } else if (response.getStatus().equals(StatusEnum.DEAD)) {
                    deadSet.add(strProxy);
                }
            }
            addCheckMap(strProxy, response);
        }
    }

    /**
     * Записать прокси в общий список провереных прокси
     * @param proxy - прокси в виде строки
     * @param response - объект с провереным прокси
     */
    private void addCheckMap(String proxy, ProxyResponse response){
        if (checkMap.containsKey(proxy)) {
            checkMap.replace(proxy, response);
            System.out.println("update proxy from checkList: " + response);
        } else {
            checkMap.put(proxy, response);
            System.out.println("add proxy from checkList: " + response);
        }
    }

    /**
     * Записать прокси в общий список провереных прокси
     * @param proxy - прокси в виде строки
     * @param response - объект с провереным прокси
     */
    private void addAliveCheckMap(String proxy, ProxyResponse response){
        if (checkAliveMap.containsKey(proxy)) {
            checkAliveMap.replace(proxy, response);
            System.out.println("update proxy from checkAliveList: " + response);
        } else {
            checkAliveMap.put(proxy, response);
            System.out.println("add proxy from checkAliveList: " + response);
        }
    }

    /**
     * Преоброзование строки в список прокси
     * @param proxyList - строка с прокси
     * @return - список с прокси
     */
    private Set<String> stringToProxyList(String proxyList){
        if (proxyList != null && !proxyList.isEmpty()) {
            String regexp = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):[0-9]+$";
            String[] proxyArr = proxyList.split("\n");
            Set<String> validProxySet = new HashSet<>();
            for (String proxy : proxyArr) {
                if (proxy.matches(regexp)) {
                    validProxySet.add(proxy);
                }
            }
            return validProxySet;
        }
        return null;
    }

    @Override
    public Map<String, ProxyResponse> getAliveCheckMap() {
        return checkAliveMap;
    }

    @Override
    public List<ProxyResponse> getCheckList() {
        return checkMap.values().stream().collect(Collectors.toList());
    }

    @Override
    public List<ProxyResponse> getAliveCheckList() {
        return checkAliveMap.values().stream().collect(Collectors.toList());
    }

    @Override
    public Set<String> getDeadSet() {
        return deadSet;
    }

    @Override
    public Map<String, ProxyResponse> getCheckMap() {
        return checkMap;
    }
}
