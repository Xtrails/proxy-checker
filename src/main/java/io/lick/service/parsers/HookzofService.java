package io.lick.service.parsers;

import io.lick.configuration.properties.HookzofServiceProperties;
import io.lick.configuration.properties.ProxyProperties;
import io.lick.service.ProxyService;
import io.lick.util.RestService;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service("hookzof")
@Scope("singleton")
public class HookzofService extends ProxyParserTemplate implements ProxyParser {

    /**
     * Обновляемый каждые UPDATE_PERIOD_TIME секунд, список прокси полученных с github hookzof
     */
    private Set<String> proxySet;

    private final RestService restService;
    private final ProxyService proxyService;
    private final HookzofServiceProperties hookzofServiceProperties;

    public HookzofService(ProxyProperties proxyProperties, RestService restService, ProxyService proxyService, HookzofServiceProperties hookzofServiceProperties) {
        super(proxyProperties);
        this.restService = restService;
        this.proxyService = proxyService;
        this.hookzofServiceProperties = hookzofServiceProperties;
    }

    @Override
    public void run() {
        updateDateTimeStart();
        log.info("===============================================");
        log.info("           Hookzof Service start");
        log.info("===============================================");
        log.info("Start Hookzof parser proxy:");
        URL proxyURL = createGetProxyURL();
        log.info("Crete link get proxy:\n" + proxyURL);
        execute(proxyURL, hookzofServiceProperties.getUpdatePeriodTime());
    }

    @Override
    public void checkProxyList() {
        proxyService.addProxyUncheckSet(proxySet);
    }

    /**
     * Получить список прокси по ссылке
     *
     * @param url - ссылка на получение офферов
     */
    @Override
    public void updateProxy(URL url) {
        Set<String> proxySetLocal = new HashSet<>();
        try {
            ResponseEntity<String> response = restService.getRestTemplate().getForEntity(url.toString(), String.class);
            Document html = Jsoup.parse(response.getBody());
            Elements elements = html.select(".blob-code");
            elements
                    .stream()
                    .filter(element -> element.text() != null && !element.text().isEmpty())
                    .forEach(element -> proxySetLocal.add(element.text()));
            if (!proxySetLocal.isEmpty()) {
                this.proxySet = proxySetLocal;
                log.info("Прокси лист успешно обновлен! Получено: " + proxySet.size());
            }
        } catch (HttpClientErrorException e) {
            log.error("Не верная ссылка: " + url + "\n" + e.getStackTrace());
        } catch (Exception e) {
            log.error("Ошибка при получении прокси: ", e);
        }
    }

    /**
     * Формирует ссылку для запроса прокси
     *
     * @return - ссылка для запроса прокси
     */
    @Override
    public URL createGetProxyURL() {
        return restService.createURL(hookzofServiceProperties.getProxyUrl(), null);
    }

    /**
     * Получить список прокси
     *
     * @return - список прокси
     */
    public Set<String> getProxySet() {
        return proxySet;
    }

    /**
     * Возвращает имя сервиса
     * @return - имя сервиса
     */
    @Override
    public String getServiceName() {
        return hookzofServiceProperties.getName();
    }
}
