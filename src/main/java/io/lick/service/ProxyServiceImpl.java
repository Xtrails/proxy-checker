package io.lick.service;

import io.lick.configuration.properties.CheckerProperties;
import io.lick.configuration.properties.ProxyServiceProperties;
import io.lick.model.FilterForm;
import io.lick.model.ProxyResponse;
import io.lick.model.enums.AnonymityEnum;
import io.lick.model.enums.StatusEnum;
import io.lick.model.enums.TypeEnum;
import io.lick.service.manager.ThreadMgr;
import io.lick.util.CheckService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProxyServiceImpl extends ThreadMgr implements ProxyService {

    // Не доступные прокси
    private Set<String> deadSet = new HashSet<>();

    // Доступные прокси
    private Set<String> aliveSet = new HashSet<>();
    private Map<String, ProxyResponse> aliveMap = new HashMap<>();

    // Не проверенные прокси
    private Set<String> uncheckSet = new HashSet<>();

    private final CheckService checkService;
    private final CheckerProperties checkerProperties;
    private final ProxyServiceProperties proxyServiceProperties;

    /**
     * Добавить прокси в список для проверки
     * @param proxySet - список прокси
     */
    @Override
    public void addProxyUncheckSet(Set<String> proxySet) {
        if (proxySet != null && !proxySet.isEmpty()) {
            uncheckSet.addAll(proxySet
                    .stream()
                    .filter(proxy -> !deadSet.contains(proxy))
                    .collect(Collectors.toList())
            );
        }
    }

    /**
     * Добавить список прокси в неактивные
     * @param deadSet - список прокси
     */
    @Override
    public void addProxyDeadSet(Set<String> deadSet) {
        if (deadSet != null && !deadSet.isEmpty()) {
            deadSet.addAll(deadSet);
        }
    }

    @Override
    public String getServiceName() {
        return proxyServiceProperties.getName();
    }

    /**
     * Получить отфильтрованный список проверенных прокси серверов
     * @param form - настройки фильтра
     * @return - отфильтрованный список проверенных прокси серверов
     */
    @Override
    public List<ProxyResponse> getProxyList(FilterForm form) {
        aliveSet.addAll(checkService.getAliveCheckMap().keySet());
        aliveMap.putAll(checkService.getAliveCheckMap());
        return filter(form);
    }

    /**
     * Фильтрует список прокси по параметрам
     * @param form - параметры фильтрации
     * @return - список отфильтрованных прокси серверов
     */
    private List<ProxyResponse> filter(FilterForm form) {
        List<ProxyResponse> proxyList = aliveMap.values().stream().collect(Collectors.toList());
        if (form != null) {

            if (form.getStatus() != null && !form.getStatus().isEmpty()) {
                Set<StatusEnum> status = new HashSet<>(form.getStatus());
                proxyList = proxyList.stream().filter(proxy -> status.contains(proxy.getStatus())).collect(Collectors.toList());
            }
            if (form.getAnonymity() != null && !form.getAnonymity().isEmpty()) {
                Set<AnonymityEnum> anonymity = new HashSet<>(form.getAnonymity());
                proxyList = proxyList.stream().filter(proxy -> anonymity.contains(proxy.getAnonymity())).collect(Collectors.toList());
            }
            if (form.getType() != null && !form.getType().isEmpty()) {
                Set<TypeEnum> type = new HashSet<>(form.getType());
                proxyList = proxyList.stream().filter(proxy -> type.contains(proxy.getType())).collect(Collectors.toList());
            }
            if (form.getCountry() != null && !form.getCountry().isEmpty()) {
                proxyList = proxyList.stream().filter(proxy -> proxy.getCountry().equals(form.getCountry())).collect(Collectors.toList());
            }
            //TODO: добавить фильтр по времени отклика
        }
        return proxyList;
    }

    @Override
    public void run() {
        while (true){
            if(!uncheckSet.isEmpty()){
                checkService.check(uncheckSet);
            }
            threadPause(checkerProperties.getUpdatePeriod());
        }
    }
}
