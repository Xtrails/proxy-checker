package io.lick.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.util.Map;

/**
 * Manages and makes a request to the API server
 */
public interface RequestAPI {

    /**
     * Takes a HttpURLConnection object and reads the web response onto a String
     * @param connection - HttpURLConnection that has been connected
     * @return Response
     * @throws IOException
     */
    RequestAPIImpl.Response getResponse(HttpURLConnection connection) throws IOException;

    /**
     * Given a proxy makes an attempt to connect to the API server, storing and returning
     * the connection and the response time in a javafx.util.Pair
     * @param proxy - The Proxy to use when attempting to connect to the API service
     * @return null if unable to connect or javafx.util.Pair
     */
    Map.Entry<HttpURLConnection, Long> connect(Proxy proxy);
}
