package io.lick.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.lick.model.enums.AnonymityEnum;
import io.lick.model.enums.StatusEnum;
import io.lick.model.enums.TypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * Прокси сервер
 */
@ApiModel(description = "Прокси сервер")
@Validated
public class ProxyResponse   {
    @JsonProperty("ip")
    private String ip = null;

    @JsonProperty("port")
    private String port = null;

    @JsonProperty("status")
    private StatusEnum status = null;

    @JsonProperty("anonymity")
    private AnonymityEnum anonymity = null;

    @JsonProperty("type")
    private TypeEnum type = null;

    @JsonProperty("country")
    private String country = null;

    @JsonProperty("response_time")
    private String responseTime = null;

    public ProxyResponse(Proxy proxy) {
        this.ip = proxy.getIp();
        this.port = String.valueOf(proxy.getPort());
        if(proxy.getProxyAnonymity()!=null)
            this.anonymity = AnonymityEnum.valueOf(proxy.getProxyAnonymity().name().toUpperCase());
        if(proxy.getProxyStatus()!=null)
            this.status = StatusEnum.valueOf(proxy.getProxyStatus().name().toUpperCase());
        this.country = proxy.getCountry();
        this.responseTime = proxy.getResponseTime();
        this.type = TypeEnum.valueOf(proxy.getProxyType().name().toUpperCase());
    }

    public ProxyResponse ip(String ip) {
        this.ip = ip;
        return this;
    }

    /**
     * IP
     * @return ip
     **/
    @ApiModelProperty(value = "IP")


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ProxyResponse port(String port) {
        this.port = port;
        return this;
    }

    /**
     * Port
     * @return port
     **/
    @ApiModelProperty(value = "Port")


    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public ProxyResponse status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
     * Status
     * @return status
     **/
    @ApiModelProperty(value = "Status")


    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public ProxyResponse anonymity(AnonymityEnum anonymity) {
        this.anonymity = anonymity;
        return this;
    }

    /**
     * Anonymity
     * @return anonymity
     **/
    @ApiModelProperty(value = "Anonymity")


    public AnonymityEnum getAnonymity() {
        return anonymity;
    }

    public void setAnonymity(AnonymityEnum anonymity) {
        this.anonymity = anonymity;
    }

    public ProxyResponse type(TypeEnum type) {
        this.type = type;
        return this;
    }

    /**
     * Type
     * @return type
     **/
    @ApiModelProperty(value = "Type")


    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public ProxyResponse country(String country) {
        this.country = country;
        return this;
    }

    /**
     * Country
     * @return country
     **/
    @ApiModelProperty(value = "Country")


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public ProxyResponse responseTime(String responseTime) {
        this.responseTime = responseTime;
        return this;
    }

    /**
     * Response time
     * @return responseTime
     **/
    @ApiModelProperty(value = "Response time")


    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProxyResponse proxyResponse = (ProxyResponse) o;
        return Objects.equals(this.ip, proxyResponse.ip) &&
                Objects.equals(this.port, proxyResponse.port) &&
                Objects.equals(this.status, proxyResponse.status) &&
                Objects.equals(this.anonymity, proxyResponse.anonymity) &&
                Objects.equals(this.type, proxyResponse.type) &&
                Objects.equals(this.country, proxyResponse.country) &&
                Objects.equals(this.responseTime, proxyResponse.responseTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, port, status, anonymity, type, country, responseTime);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ProxyResponse {\n");

        sb.append("    ip: ").append(toIndentedString(ip)).append("\n");
        sb.append("    port: ").append(toIndentedString(port)).append("\n");
        sb.append("    status: ").append(toIndentedString(status)).append("\n");
        sb.append("    anonymity: ").append(toIndentedString(anonymity)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    country: ").append(toIndentedString(country)).append("\n");
        sb.append("    responseTime: ").append(toIndentedString(responseTime)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
