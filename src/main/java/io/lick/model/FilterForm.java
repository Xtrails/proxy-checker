package io.lick.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.lick.model.enums.AnonymityEnum;
import io.lick.model.enums.StatusEnum;
import io.lick.model.enums.TypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Прокси сервер
 */
@ApiModel(description = "Прокси сервер")
@Validated

public class FilterForm   {

  @JsonProperty("status")
  @Valid
  private List<StatusEnum> status = null;

  @JsonProperty("anonymity")
  @Valid
  private List<AnonymityEnum> anonymity = null;

  @JsonProperty("type")
  @Valid
  private List<TypeEnum> type = null;

  @JsonProperty("country")
  private String country = null;

  @JsonProperty("response_time_from")
  private String responseTimeFrom = null;

  @JsonProperty("response_time_to")
  private String responseTimeTo = null;

  public FilterForm status(List<StatusEnum> status) {
    this.status = status;
    return this;
  }

  public FilterForm addStatusItem(StatusEnum statusItem) {
    if (this.status == null) {
      this.status = new ArrayList<StatusEnum>();
    }
    this.status.add(statusItem);
    return this;
  }

  /**
   * Status
   * @return status
   **/
  @ApiModelProperty(value = "Status")


  public List<StatusEnum> getStatus() {
    return status;
  }

  public void setStatus(List<StatusEnum> status) {
    this.status = status;
  }

  public FilterForm anonymity(List<AnonymityEnum> anonymity) {
    this.anonymity = anonymity;
    return this;
  }

  public FilterForm addAnonymityItem(AnonymityEnum anonymityItem) {
    if (this.anonymity == null) {
      this.anonymity = new ArrayList<AnonymityEnum>();
    }
    this.anonymity.add(anonymityItem);
    return this;
  }

  /**
   * Anonymity
   * @return anonymity
   **/
  @ApiModelProperty(value = "Anonymity")


  public List<AnonymityEnum> getAnonymity() {
    return anonymity;
  }

  public void setAnonymity(List<AnonymityEnum> anonymity) {
    this.anonymity = anonymity;
  }

  public FilterForm type(List<TypeEnum> type) {
    this.type = type;
    return this;
  }

  public FilterForm addTypeItem(TypeEnum typeItem) {
    if (this.type == null) {
      this.type = new ArrayList<TypeEnum>();
    }
    this.type.add(typeItem);
    return this;
  }

  /**
   * Type
   * @return type
   **/
  @ApiModelProperty(value = "Type")


  public List<TypeEnum> getType() {
    return type;
  }

  public void setType(List<TypeEnum> type) {
    this.type = type;
  }

  public FilterForm country(String country) {
    this.country = country;
    return this;
  }

  /**
   * Country
   * @return country
   **/
  @ApiModelProperty(value = "Country")


  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public FilterForm responseTimeFrom(String responseTimeFrom) {
    this.responseTimeFrom = responseTimeFrom;
    return this;
  }

  /**
   * Response time from
   * @return responseTimeFrom
   **/
  @ApiModelProperty(value = "Response time from")


  public String getResponseTimeFrom() {
    return responseTimeFrom;
  }

  public void setResponseTimeFrom(String responseTimeFrom) {
    this.responseTimeFrom = responseTimeFrom;
  }

  public FilterForm responseTimeTo(String responseTimeTo) {
    this.responseTimeTo = responseTimeTo;
    return this;
  }

  /**
   * Response time to
   * @return responseTimeTo
   **/
  @ApiModelProperty(value = "Response time to")


  public String getResponseTimeTo() {
    return responseTimeTo;
  }

  public void setResponseTimeTo(String responseTimeTo) {
    this.responseTimeTo = responseTimeTo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FilterForm filterForm = (FilterForm) o;
    return Objects.equals(this.status, filterForm.status) &&
            Objects.equals(this.anonymity, filterForm.anonymity) &&
            Objects.equals(this.type, filterForm.type) &&
            Objects.equals(this.country, filterForm.country) &&
            Objects.equals(this.responseTimeFrom, filterForm.responseTimeFrom) &&
            Objects.equals(this.responseTimeTo, filterForm.responseTimeTo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, anonymity, type, country, responseTimeFrom, responseTimeTo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FilterForm {\n");

    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    anonymity: ").append(toIndentedString(anonymity)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    responseTimeFrom: ").append(toIndentedString(responseTimeFrom)).append("\n");
    sb.append("    responseTimeTo: ").append(toIndentedString(responseTimeTo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
