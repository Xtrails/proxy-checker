package io.lick.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Сервис для создания REST запросов
 *
 * Пример использования:
 * https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-resttemplate.html
 *
 * Гайд по Rest Template. GET POST запросы.
 * https://www.baeldung.com/rest-template
 */

@Slf4j
@Service
public class RestServiceImpl implements RestService {

    private final RestTemplate restTemplate;

    public RestServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * Формирует ссылку для GET запроса
     * @param link - ссылка на ресурс
     * @param params - параметры GET запроса
     * @return - параметризованная ссылка для GET запроса
     */
    public URL createURL(String link, Map<String,String> params) {
        if (!link.isEmpty() && params != null && params.size() > 0) {
            link += "?";
            for (Map.Entry<String, String> entry : params.entrySet()) {
                link += entry.getKey() + "=" + entry.getValue() + "&";
            }
            link = link.substring(0, link.length() - 1);
        }
        try {
            return new URL(link);
        } catch (MalformedURLException e) {
            log.error("Ошибка при создании URL. " + e.getMessage());
        }
        return null;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return this.restTemplate;
    }
}
