package io.lick.configuration.properties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
@ConfigurationProperties(prefix = "checker")
public class CheckerProperties {
    private Integer maxThreads;
    private Integer timeout;
    private String userIp;
    private String apiUrl;
    private Integer updatePeriod;
}
