package io.lick.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.lick.model.FilterForm;
import io.lick.model.ProxyResponse;
import io.lick.service.ProxyService;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class ProxyApiController implements ProxyApi {

    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    private ProxyService proxyService;

    public ProxyApiController(ObjectMapper objectMapper, HttpServletRequest request, ProxyService proxyService) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.proxyService = proxyService;
    }

    public ResponseEntity<List<ProxyResponse>> getProxyList(@ApiParam(value = "Filter form" ,required=true )  @Valid @RequestBody FilterForm filterForm) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            List<ProxyResponse> proxyList = proxyService.getProxyList(filterForm);
             return new ResponseEntity<List<ProxyResponse>>(proxyList, HttpStatus.OK);
        }
        return new ResponseEntity<List<ProxyResponse>>(HttpStatus.NOT_IMPLEMENTED);
    }

}
