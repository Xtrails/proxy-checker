package io.lick.service.parsers;

import io.lick.configuration.properties.FreeProxyListServiceProperties;
import io.lick.configuration.properties.ProxyProperties;
import io.lick.service.ProxyService;
import io.lick.util.RestService;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service("free-proxy-list")
@Scope("singleton")
public class FreeProxyListService extends ProxyParserTemplate implements ProxyParser {

    /**
     * Обновляемый каждые UPDATE_PERIOD_TIME секунд, список прокси полученных с free-proxy-list
     */
    private Set<String> proxySet;

    private final RestService restService;
    private final ProxyService proxyService;
    private final FreeProxyListServiceProperties freeProxyListServiceProperties;

    public FreeProxyListService(ProxyProperties proxyProperties, RestService restService, ProxyService proxyService, FreeProxyListServiceProperties freeProxyListServiceProperties) {
        super(proxyProperties);
        this.restService = restService;
        this.proxyService = proxyService;
        this.freeProxyListServiceProperties = freeProxyListServiceProperties;
    }

    @Override
    public void run() {
        updateDateTimeStart();
        log.info("===============================================");
        log.info("           Free Proxy-list Service start");
        log.info("===============================================");
        log.info("Start Free Proxy-list parser proxy:");
        URL proxyURL = createGetProxyURL();
        log.info("Crete link get proxy:\n" + proxyURL);
        execute(proxyURL, freeProxyListServiceProperties.getUpdatePeriodTime());
    }

    @Override
    public void checkProxyList() {
        proxyService.addProxyUncheckSet(proxySet);
    }

    /**
     * Получить список прокси по ссылке
     *
     * @param url - ссылка на получение офферов
     */
    @Override
    public void updateProxy(URL url) {
        Set<String> proxySetLocal = new HashSet<>();
        try {
            HttpHeaders header = new HttpHeaders();
            header.set("User-Agent","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36");

            ResponseEntity<String> response = restService.getRestTemplate().getForEntity(url.toString(), String.class);
            Document html = Jsoup.parse(response.getBody());
            Elements elements = html.select(".blob-code");
            elements
                    .stream()
                    .filter(element -> element.text() != null && !element.text().isEmpty())
                    .forEach(element -> proxySetLocal.add(element.text()));
            if (!proxySetLocal.isEmpty()) {
                this.proxySet = proxySetLocal;
                log.info("Прокси лист успешно обновлен! Получено: " + proxySet.size());
            }
        } catch (HttpClientErrorException e) {
            log.error("Не верная ссылка: " + url + "\n" + e.getStackTrace());
        } catch (Exception e) {
            log.error("Ошибка при получении прокси: ", e);
        }
    }

    /**
     * Формирует ссылку для запроса прокси
     *
     * @return - ссылка для запроса прокси
     */
    @Override
    public URL createGetProxyURL() {
        return restService.createURL(freeProxyListServiceProperties.getProxyUrl(), null);
    }

    /**
     * Получить список прокси
     *
     * @return - список прокси
     */
    public Set<String> getProxySet() {
        return proxySet;
    }

    /**
     * Возвращает имя сервиса
     * @return - имя сервиса
     */
    @Override
    public String getServiceName() {
        return freeProxyListServiceProperties.getName();
    }
}
