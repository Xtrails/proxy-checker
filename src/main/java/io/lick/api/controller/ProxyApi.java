package io.lick.api.controller;

import io.lick.model.FilterForm;
import io.lick.model.ProxyResponse;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

@Api(value = "proxy", description = "the proxy API")
public interface ProxyApi {

    @ApiOperation(value = "Проверяет работоспособность прокси серверов", nickname = "getProxyList", notes = "", response = ProxyResponse.class, responseContainer = "List", tags={ "All", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = ProxyResponse.class, responseContainer = "List") })
    @RequestMapping(value = "/proxy",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<List<ProxyResponse>> getProxyList(@ApiParam(value = "Filter form", required = true) @Valid @RequestBody FilterForm body);

}
