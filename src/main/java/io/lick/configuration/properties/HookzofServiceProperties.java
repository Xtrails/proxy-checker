package io.lick.configuration.properties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
@ConfigurationProperties(prefix = "proxy.services.hookzof-service")
public class HookzofServiceProperties {
    private String name;
    private String proxyUrl;
    private Integer updatePeriodTime;
}
