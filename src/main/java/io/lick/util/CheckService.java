package io.lick.util;

import io.lick.model.ProxyResponse;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CheckService {

    /**
     * Проверить список прокси адресов на доступность
     * @param proxyList - список прокси серверов
     */
    void check(Set<String> proxyList);

    /**
     * Проверить список прокси адресов на доступность
     * @param proxyList - список прокси серверов
     */
    void check(String proxyList);

    /**
     * Получить список провереных прокси
     * @return - список провереных прокси
     */
    Map<String, ProxyResponse> getCheckMap();

    /**
     * Получить список активных прокси
     * @return - список активных прокси
     */
    Map<String, ProxyResponse> getAliveCheckMap();

    /**
     * Получить список провереных прокси
     * @return - список провереных прокси
     */
    List<ProxyResponse> getCheckList();

    /**
     * Получить список активных прокси
     * @return - список активных прокси
     */
    List<ProxyResponse> getAliveCheckList();

    /**
     * Получить список неработающих прокси
     * @return - список неработающих прокси
     */
    Set<String> getDeadSet();
}
