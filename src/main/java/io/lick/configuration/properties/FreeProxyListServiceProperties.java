package io.lick.configuration.properties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
@ConfigurationProperties(prefix = "proxy.services.free-proxy-list-service")
public class FreeProxyListServiceProperties {
    private String name;
    private String proxyUrl;
    private Integer updatePeriodTime;
}
