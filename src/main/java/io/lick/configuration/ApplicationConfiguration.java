package io.lick.configuration;

import io.lick.service.manager.ServiceMgr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {
    @Autowired
    ServiceMgr serviceMgr;

    @Bean("service-manager")
    public ServiceMgr initServiceMgr(){
        serviceMgr.getAllServices().values().forEach(service -> {
            service.setThreadName(service.getServiceName());
        });
        serviceMgr.getProxyService().setThreadName(serviceMgr.getProxyService().getServiceName());
        return serviceMgr;
    }
}
