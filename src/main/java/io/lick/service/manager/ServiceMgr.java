package io.lick.service.manager;

import io.lick.service.ProxyService;
import io.lick.service.parsers.ProxyParser;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Класс выбора и выдачи прокси парсеров
 */

@Component
public class ServiceMgr {
    private Map<String, ProxyParser> proxyParsers;
    private ProxyService proxyService;

    public ServiceMgr(Map<String, ProxyParser> proxyParsers, ProxyService proxyService) {
        this.proxyParsers = proxyParsers;
        this.proxyService = proxyService;
    }

    /**
     * Получить список со всеми сервисами
     *
     * @return - список со всеми сервисами
     */
    public Map<String, ProxyParser> getAllServices() {
        return proxyParsers;
    }

    /**
     * Получить сервис по нозванию
     *
     * @param name - название сервиса
     * @return - сервис
     */
    public ProxyParser getServiceByName(String name) {
        if (proxyParsers.containsKey(name)) {
            return proxyParsers.get(name);
        }
        return null;
    }

    /**
     * Получить прокси листы со всех сайтов
     *
     * @return - прокси листы со всех сайтов
     */
    private Set<String> getAllProxy() {
        Set<String> set = new HashSet<>();
        if (proxyParsers != null && !proxyParsers.isEmpty()) {
            for (ProxyParser service : proxyParsers.values()) {
                Set<String> serviceOffers = service.getProxySet();
                if (serviceOffers != null && !serviceOffers.isEmpty()) {
                    set.addAll(serviceOffers);
                }
            }
        }
        return set;
    }

    /**
     * Возвращает парсер по параметру
     *
     * @param parserName - название сайта
     * @return - список офферов
     */
    public Set<String> getProxy(String parserName) {
        Set<String> set = new HashSet<>();
        if (parserName != null && !parserName.isEmpty()) {
            if (parserName.equals("all"))
                return getAllProxy();
            else if (proxyParsers.containsKey(parserName))
                return proxyParsers.get(parserName).getProxySet();
        }
        return set;
    }

    public ProxyService getProxyService() {
        return proxyService;
    }
}

