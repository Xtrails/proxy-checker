package io.lick.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets anonymity
 */
public enum AnonymityEnum {
    TRANSPARENT("TRANSPARENT"),

    ANONYMOUS("ANONYMOUS"),

    ELITE("ELITE");

    private String value;

    AnonymityEnum(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static AnonymityEnum fromValue(String text) {
        for (AnonymityEnum b : AnonymityEnum.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}
